# Maui Kamaole B-208

**Check-in time:** 3:00 PM

**Check-out time:** 11:00 AM

**Lockbox code:** 1287

**Pool access code:** 1234

**WiFi network:** mkB208

**WiFi password:** kamaoleB7

-------------------------------------------

Aloha and Welcome Guests,

Please follow the steps below to check in.
If you have any questions or need to reach us for any reason, please email us at
hyeyoungshinw@gmail.com or call us at: 212-535-8565.


## Check-in procedure

1. **Check in at the front desk**   
   Upon arriving at 2777 South Kihei Road, as you head up the steep driveway, veer 
   right and then left to go around a big tree in the middle of the driveway, and
   follow the signs to toward the B units. You will soon see the front desk and 
   pool on your left.  Park in the short term parking lot and check in at the
   front desk or main office. (Front desk phone: 808-874-8467).  
   
   The front desk is open 8am--12pm and 1pm--4:45pm.  If you arrive outside of
   these hours, proceed to step 3, and return to the front desk to check-in when
   it reopens.
   
2. **Parking**   
   Upon check-in at the front desk, you will be given a parking pass for 
   our **reserved parking stall: B7**. 

3. **Entering the Unit**   
   Park your car in stall B7 and head up the stairs to **unit B208**.
   You will find a lockbox hanging on the front door knob.  Open this box with 
   the code 1287.  Use the key inside the box to let yourself in, and then 
   **please return the key to the lockbox** immediately after using it to enter
   for the first time.  You will need to enter the same code (1287) to close 
   the lockbox. 
   
4. Once you enter the condo, there should be two keys hanging on the wall on
   the left just inside the entryway.  Please use these keys during your stay. 
   Do not use the lockbox key unless you lock yourself out.
   
5. When you check out, please be sure to leave the two keys inside the unit,
   hanging from the key rack. If you lose a key, a $50 replacement fee will be 
   deducted from your deposit. 


---------------------------------------------------------


**Housekeeping**
Unless you made special arrangements, maid service is not normally provided.
Our cleaners work hard to ensure that the unit is very clean before guests
arrive.  If upon your arrival you notice something that seems to
have been missed, please contact us at 212-535-8565, or if you cannot reach us
you may try our housekeeping director Melinda at 808-280-2878.


---------------------------------------------------

## House rules

+ Please leave your shoes at the front door.  It is Hawaiian custom and the floor
  is cleaned so that it is safe for you and your family to walk barefoot. 

+ As a courtesy to our neighbors, please refrain from using the washer and dryer after 10PM. 

+ Please try to help us conserve electricity by turning off the air conditioner,
  fan, and lights when possible, especially upon leaving the unit.

+ Our unit is strictly **Non-smoking**.

+ If you spill on linens or the furniture, please let us know.  Do not try to
  hide it. It is much harder to get the stain out after a period of time. 
  
Thank you for your consideration in observing our house rules.


## Recommended Local Spots

Just a 15 minute walk (take the footpath north-west through the 
property that takes you down to South Kihei Road) there are excellent
restaurants and small convenience shops at Rainbow plaza.  We recommend 808
Deli, Coconut's and Cafe Ole.

Walking about 10 minutes further north along South Kihei Road and you will
arrive at another favorite, the Paia Fish Market, which serves fresh fish. Just
beyond Paia Fish Market is Foodland, which is a full service grocery store.  If
you prefer Safeway, that is about a 10 to 15 minute drive north from the condo.

Just across the street from our condo, and a little bit south is a very nice
restaurant called "5 Palms."  (A bit further south is "Sorento's on the
Beach," which is also very good, but quite expensive.)  
There is a very small market called "Zach's Deli and General Store" near 5 Palms,
and if you just need a few essentials, that is the closest market to our unit.